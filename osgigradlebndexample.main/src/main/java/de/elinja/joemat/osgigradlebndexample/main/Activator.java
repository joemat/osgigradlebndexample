package de.elinja.joemat.osgigradlebndexample.main;

import java.util.Collection;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;

import de.elinja.joemat.osgigradlebndexample.api.TwoNumberCalculator;

public class Activator implements BundleActivator {

	@Override
	public void start(BundleContext context) throws Exception {
		System.out.println("Starting OSGiGradleBndExample");

		Runnable runnable = new Runnable() {

			@Override
			public void run() {
				int parameter1 = 4711;
				int parameter2 = 815;

				callAllTwoNumberCalculators(context, parameter1, parameter2);
				stopFramework(context);
			}

			private void callAllTwoNumberCalculators(BundleContext context, int parameter1, int parameter2) {
				try {
					Collection<ServiceReference<TwoNumberCalculator>> serviceRefs = context
							.getServiceReferences(TwoNumberCalculator.class, null);

					serviceRefs.forEach(serviceRef -> {
						TwoNumberCalculator service = context.getService(serviceRef);
						if (service != null) {
							System.out.println(service.getName() + " => " + service.calc(parameter1, parameter2));
						}
						context.ungetService(serviceRef);
					});
				} catch (InvalidSyntaxException e) {
					e.printStackTrace();
				}
			}

			private void stopFramework(BundleContext context) {
				try {
					context.getBundle(0).stop();
				} catch (BundleException e) {
					e.printStackTrace();
				}
			}
		};
		
		new Thread(runnable).start();
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		System.out.println("Stopping OSGiGradleBndExample");

	}

}
