package de.elinja.joemat.osgigradlebndexample.calc.addition;

import org.osgi.service.component.annotations.*;

import de.elinja.joemat.osgigradlebndexample.api.TwoNumberCalculator;

@Component
public class TwoNumberAddition implements TwoNumberCalculator 
{

	@Override
	public String getName() {
		return "Addition:";
	}

	@Override
	public int calc(int parameter1, int parameter2) {
		return parameter1 + parameter2;
	}
}
