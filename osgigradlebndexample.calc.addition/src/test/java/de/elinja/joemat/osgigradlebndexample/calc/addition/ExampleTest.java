package de.elinja.joemat.osgigradlebndexample.calc.addition;

import static org.junit.Assert.*;

import org.junit.Test;

public class ExampleTest {

	@Test
	public void testCalcAdd() {
		TwoNumberAddition adder = new TwoNumberAddition();
		assertEquals(3, adder.calc(1, 2));
	}

}
